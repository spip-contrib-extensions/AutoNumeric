<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// j
	'autonumeric_description' => 'Encapsulation de la librairie autoNumeric',
	'jquery_number_slogan' => 'Formatez vos nombres !'
);
